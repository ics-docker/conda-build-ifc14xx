FROM registry.esss.lu.se/ics-docker/conda-build:3.18.11-2

USER root

# Install extra dependencies required to install the ifc14xx toolchain
RUN yum install -y \
      file \
      # execstack required for openssl
      prelink \
      # makeinfo required for libffi
      texinfo \
      # gcc required by toolchain script
      gcc-c++ \
  && yum clean all \
  && rm -rf /var/cache/yum

# cross-compiler version
ENV E3_CROSS_COMPILER_VERSION=stable \
    E3_CROSS_COMPILER_KERNEL=2.6-4.14 \
    E3_CROSS_COMPILER_SHORT_SHA=ea12a175

# Install ifc14xx toolchain
ENV IFC14XX_TOOLCHAIN_SCRIPT ifc14xx-glibc-x86_64-ifc14xx-toolchain-ppc64e6500-toolchain-${E3_CROSS_COMPILER_KERNEL}-${E3_CROSS_COMPILER_SHORT_SHA}.sh
RUN wget --quiet -P /tmp https://artifactory.esss.lu.se/artifactory/yocto/toolchain/${E3_CROSS_COMPILER_VERSION}/${IFC14XX_TOOLCHAIN_SCRIPT} \
  && chmod a+x /tmp/${IFC14XX_TOOLCHAIN_SCRIPT} \
  && /tmp/${IFC14XX_TOOLCHAIN_SCRIPT} -y \
  && rm -f /tmp/${IFC14XX_TOOLCHAIN_SCRIPT}

# Remove perl to use the one installed with conda
# Some modules are missing (FindBin.pm) in the toolchain
RUN mv /opt/ifc14xx/2.6-4.14/sysroots/x86_64-fslsdk-linux/usr/bin/perl /opt/ifc14xx/2.6-4.14/sysroots/x86_64-fslsdk-linux/usr/bin/perl.org

USER conda

# patch conda & conda-build
COPY conda-ppc64-support.patch /tmp/
RUN cd /opt/conda/lib/python3.7/site-packages && patch -p1 < /tmp/conda-ppc64-support.patch
COPY conda-build-ppc64-support.patch /tmp/
RUN cd /opt/conda/lib/python3.7/site-packages && patch -p1 < /tmp/conda-build-ppc64-support.patch

# Should be passed with -m to conda-build to cross-compile
# (will overwrite defaults from conda_build_config.yaml downloaded in the entrypoint)
COPY conda_build_config_ppc64.yaml /home/conda/

WORKDIR /build

ENTRYPOINT [ "/opt/conda/bin/tini", "--", "/entrypoint" ]
