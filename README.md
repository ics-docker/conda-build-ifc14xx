# conda-build-ifc14xx

[Docker](https://www.docker.com) image to cross-compile conda packages for ifc14xx.

## Building

This image is built automatically by gitlab-ci.

## How to use this image

To create a conda package, you must write a conda build recipe: https://conda.io/docs/user-guide/tutorials/build-pkgs.html
